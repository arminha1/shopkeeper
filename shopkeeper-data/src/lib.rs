#![forbid(unsafe_code)]

mod money;

pub use crate::money::Money;
use base64::{engine::general_purpose::STANDARD, Engine};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use uuid::Uuid;

#[derive(Debug, Deserialize, Serialize)]
pub struct PurchaseHeader {
    pub id: Uuid,
    #[serde(flatten)]
    pub data: PurchaseData,
    pub total: Money,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum ImportPurchaseResult {
    Created { id: Uuid },
    Duplicate,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct ItemData {
    pub name: String,
    pub amount: f64,
    pub price: Money,
    pub original_price: Option<Money>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Clone)]
pub struct PurchaseData {
    pub store: String,
    pub location: String,
    pub purchase_time: DateTime<Utc>,
}

/// Struct for creating a new Purchase.
#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct NewPurchase {
    #[serde(flatten)]
    pub data: PurchaseData,
    pub items: Vec<ItemData>,
    pub attachments: Vec<Attachment>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Clone)]
pub struct AttachmentHeader {
    pub id: Uuid,
    pub name: String,
    pub content_type: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Clone)]
pub struct Attachment {
    pub name: String,
    pub content_type: String,
    #[serde(deserialize_with = "from_base64", serialize_with = "as_base64")]
    pub data: Vec<u8>,
}

fn as_base64<T, S>(data: &T, serializer: S) -> Result<S::Ok, S::Error>
where
    T: AsRef<[u8]>,
    S: Serializer,
{
    serializer.serialize_str(&STANDARD.encode(data.as_ref()))
}

fn from_base64<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
where
    D: Deserializer<'de>,
{
    use serde::de::Error;
    String::deserialize(deserializer).and_then(|string| {
        STANDARD
            .decode(string)
            .map_err(|err| Error::custom(err.to_string()))
    })
}
