use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::iter::Sum;
use std::ops::{Add, AddAssign, Sub};
use std::str::FromStr;

/// Newtype for money values in CHF.
#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq)]
#[repr(transparent)]
#[serde(transparent)]
pub struct Money {
    value: f64,
}

impl Money {
    pub const ZERO: Self = Self { value: 0f64 };

    pub fn abs(self) -> Self {
        self.value.abs().into()
    }
}

impl Add for Money {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let sum = self.value + rhs.value;
        sum.into()
    }
}

impl AddAssign for Money {
    fn add_assign(&mut self, rhs: Self) {
        let sum = self.value + rhs.value;
        self.value = round_currency(sum);
    }
}

impl Sub for Money {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        let diff = self.value - rhs.value;
        diff.into()
    }
}

impl Sum for Money {
    fn sum<I: Iterator<Item = Money>>(iter: I) -> Self {
        let mut accumulator = Money::ZERO;
        for m in iter {
            accumulator += m;
        }
        accumulator
    }
}

impl FromStr for Money {
    type Err = std::num::ParseFloatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<f64>().map(Into::into)
    }
}

impl Display for Money {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:.2}", self.value)
    }
}

impl From<f64> for Money {
    fn from(value: f64) -> Self {
        Self {
            value: round_currency(value),
        }
    }
}

impl From<Money> for f64 {
    fn from(money: Money) -> Self {
        money.value
    }
}

fn round_currency(value: f64) -> f64 {
    (value * 100.0).round() / 100.0
}

#[cfg(test)]
mod tests {

    use proptest::proptest;

    use super::*;

    #[test]
    fn round_currency_rounds_with_2_precision() {
        assert_eq!(round_currency(0.0049f64), 0f64);
        assert_eq!(round_currency(0.005f64), 0.01f64);
        assert_eq!(round_currency(200.016f64), 200.02f64);
    }

    proptest! {
        #[test]
        fn money_can_represent_any_value_up_to_1_000_000(v in -1_000_000_00i32 ..= 1_000_000_00) {
            let value = v as f64 / 100f64;
            let money: Money = value.into();
            assert_eq!(value, money.value);
        }
    }
}
