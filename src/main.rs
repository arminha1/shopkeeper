#![forbid(unsafe_code)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

mod app;
mod data;
mod db;
mod error;
mod icons;

use crate::data::{CsvRow, ItemResult, MonthlySpending, NewPurchase, PurchaseHeader, YearMonth};
use crate::error::{CsvSerializationError, WebError};
use actix_web::web::{self, Data, Json, Query};
use actix_web::{get, middleware, post, App, HttpResponse, HttpServer, Responder};
use app::{ReadConnection, WriteConnection};
use chrono::{DateTime, TimeZone, Utc};
use chrono_tz::Europe::Zurich;
use csv::Writer;
use db::DbPool;
use log::info;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use shopkeeper_data::Money;
use std::collections::BTreeMap;
use std::env;
use std::fmt::Write;
use tinytemplate::TinyTemplate;
use uuid::Uuid;

const PURCHASE: &str = include_str!("assets/purchase.html");
const PURCHASE_DETAIL: &str = include_str!("assets/purchase_detail.html");
const ITEM_SEARCH_RESULTS: &str = include_str!("assets/item_search_results.html");
const MONTHLY_SPENDING: &str = include_str!("assets/spending.html");
const CSS_STYLE: &str = include_str!("assets/style.css");

const MAX_JSON_PAYLOAD: usize = 1024 * 1024;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    dotenvy::dotenv().ok();
    env_logger::init();

    let (listen_ip, port) = get_bind_address()?;

    let pool = setup_connection_pool()?;
    pool.run_migrations()?;

    info!("Starting server at http://{}:{}", listen_ip, port);
    HttpServer::new(move || {
        let mut tt = TinyTemplate::new();
        tt.add_template("purchase.html", PURCHASE).unwrap();
        tt.add_template("purchase_detail.html", PURCHASE_DETAIL)
            .unwrap();
        tt.add_template("item_search_results.html", ITEM_SEARCH_RESULTS)
            .unwrap();
        tt.add_template("spending.html", MONTHLY_SPENDING).unwrap();
        tt.add_formatter("float_formatter", format_float);
        tt.add_formatter("timestamp_formatter", format_timestamp);

        App::new()
            .app_data(Data::new(tt))
            .app_data(Data::new(pool.clone()))
            .wrap(middleware::Logger::default())
            .app_data(Data::new(
                web::JsonConfig::default().limit(MAX_JSON_PAYLOAD),
            ))
            .service(index)
            .service(list_purchases_html)
            .service(list_purchases)
            .service(get_purchase_html)
            .service(get_purchase)
            .service(import_purchase)
            .service(get_attachment)
            .service(search_item_html)
            .service(search_item)
            .service(export_csv)
            .service(get_monthly_spending_html)
            .service(get_monthly_spending)
            .service(get_last_purchase)
            .service(style_css)
            .service(icons::favicon)
            .service(icons::favicon16)
            .service(icons::favicon32)
            .service(icons::apple_touch_icon)
            .service(icons::safari_pinned_tab)
            .service(icons::site_webmanifest)
            .service(icons::android_chrome)
            .service(icons::android_chrome_large)
    })
    .bind((listen_ip.as_str(), port))?
    .run()
    .await?;
    Ok(())
}

fn format_float(value: &Value, output: &mut String) -> tinytemplate::error::Result<()> {
    if let serde_json::value::Value::Number(num) = value {
        let num = num.as_f64().unwrap();
        write!(output, "{:.2}", num)?;
        Ok(())
    } else {
        tinytemplate::format(value, output)
    }
}

fn format_timestamp(value: &Value, output: &mut String) -> tinytemplate::error::Result<()> {
    if let serde_json::value::Value::String(timestamp) = value {
        if let Ok(timestamp) = timestamp.parse::<DateTime<Utc>>() {
            let timestamp = Zurich.from_utc_datetime(&timestamp.naive_utc());
            write!(output, "{}", timestamp.naive_local())?;
            return Ok(());
        }
    }
    tinytemplate::format(value, output)
}

fn setup_connection_pool() -> anyhow::Result<DbPool> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = DbPool::new(database_url)?;
    Ok(pool)
}

fn get_bind_address() -> anyhow::Result<(String, u16)> {
    let listen_ip = env::var("LISTEN_IP").unwrap_or_else(|_| "127.0.0.1".to_owned());
    let port = match env::var("PORT") {
        Ok(p) => p.parse()?,
        _ => 8080,
    };
    Ok((listen_ip, port))
}

#[get("/")]
async fn index() -> impl Responder {
    let index = include_str!("assets/index.html");
    HttpResponse::Ok().content_type("text/html").body(index)
}

#[get("/style.css")]
async fn style_css() -> impl Responder {
    let style = CSS_STYLE;
    HttpResponse::Ok().content_type("text/css").body(style)
}

#[get("/purchase")]
async fn list_purchases_html(
    tmpl: web::Data<TinyTemplate<'_>>,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;

    let purchases = web::block(move || conn.load_purchases()).await??;

    render_html(&tmpl, "purchase.html", &purchases)
}

#[get("/api/purchase")]
async fn list_purchases(pool: web::Data<DbPool>) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;
    let purchases = web::block(move || conn.load_purchases()).await??;
    Ok(HttpResponse::Ok().json(purchases))
}

#[get("/purchase/{purchase_id}")]
async fn get_purchase_html(
    tmpl: web::Data<TinyTemplate<'_>>,
    pool: web::Data<DbPool>,
    purchase_id: web::Path<Uuid>,
) -> Result<HttpResponse, WebError> {
    let purchase_id = purchase_id.into_inner();
    let mut conn = pool.get_read_connection()?;

    let purchase = web::block(move || conn.load_purchase(purchase_id)).await??;
    let response = match purchase {
        Some(purchase) => render_html(&tmpl, "purchase_detail.html", &purchase)?,
        None => HttpResponse::NotFound().finish(),
    };
    Ok(response)
}

#[get("/api/purchase/{purchase_id}")]
async fn get_purchase(
    pool: web::Data<DbPool>,
    purchase_id: web::Path<Uuid>,
) -> Result<HttpResponse, WebError> {
    let purchase_id = purchase_id.into_inner();
    let mut conn = pool.get_read_connection()?;

    let purchase = web::block(move || conn.load_purchase(purchase_id)).await??;
    let response = match purchase {
        Some(purchase) => HttpResponse::Ok().json(purchase),
        None => HttpResponse::NotFound().finish(),
    };
    Ok(response)
}

#[get("/monthly-spending")]
async fn get_monthly_spending_html(
    tmpl: web::Data<TinyTemplate<'_>>,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;

    let purchases = web::block(move || conn.load_purchases()).await??;
    let spending = monthly_spending(purchases);
    render_html(&tmpl, "spending.html", &spending)
}

#[get("/api/spending/monthly")]
async fn get_monthly_spending(pool: web::Data<DbPool>) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;

    let purchases = web::block(move || conn.load_purchases()).await??;
    let spending = monthly_spending(purchases);
    Ok(HttpResponse::Ok().json(spending))
}

#[derive(Debug, Deserialize)]
struct LastPurchaseParameters {
    store: Option<String>,
}

#[get("/api/last-purchase")]
async fn get_last_purchase(
    pool: web::Data<DbPool>,
    params: web::Query<LastPurchaseParameters>,
) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;
    let store = params.into_inner().store;

    let purchase = web::block(move || conn.load_last_purchase(store.as_deref())).await??;

    let response = match purchase {
        Some(purchase) => HttpResponse::Ok().json(purchase),
        None => HttpResponse::NotFound().finish(),
    };
    Ok(response)
}

fn monthly_spending(purchases: Vec<PurchaseHeader>) -> Vec<MonthlySpending> {
    let mut spending = BTreeMap::<YearMonth, Money>::new();
    for purchase in purchases {
        let month = YearMonth::from_date(purchase.data.purchase_time.date_naive());
        *spending.entry(month).or_insert(Money::ZERO) += purchase.total;
    }
    spending
        .into_iter()
        .rev()
        .map(|(month, total)| MonthlySpending::new(month, total))
        .collect()
}

#[derive(Debug, Deserialize)]
struct Search {
    name: String,
}

#[derive(Debug, Serialize)]
struct SearchResults {
    name: String,
    items: Vec<ItemResult>,
}

#[get("/search/item")]
async fn search_item_html(
    tmpl: web::Data<TinyTemplate<'_>>,
    pool: web::Data<DbPool>,
    search: Query<Search>,
) -> Result<HttpResponse, WebError> {
    let search = search.into_inner();
    let mut conn = pool.get_read_connection()?;

    let name = search.name.clone();
    let items = web::block(move || conn.search_items(&name)).await??;

    let results = SearchResults {
        name: search.name,
        items,
    };
    render_html(&tmpl, "item_search_results.html", &results)
}

#[get("/api/search/item")]
async fn search_item(
    pool: web::Data<DbPool>,
    search: Query<Search>,
) -> Result<HttpResponse, WebError> {
    let search = search.into_inner();
    let mut conn = pool.get_read_connection()?;
    let items = web::block(move || conn.search_items(&search.name)).await??;
    Ok(HttpResponse::Ok().json(items))
}

#[get("/attachment/{attachment_id}/{attachment_name}")]
async fn get_attachment(
    pool: web::Data<DbPool>,
    params: web::Path<(Uuid, String)>,
) -> Result<HttpResponse, WebError> {
    let (attachment_id, attachment_name) = params.into_inner();
    let mut conn = pool.get_read_connection()?;

    let attachment = web::block(move || conn.load_attachment(attachment_id)).await??;
    let response = match attachment {
        Some(attachment) if attachment.name == attachment_name => HttpResponse::Ok()
            .content_type(attachment.content_type)
            .body(attachment.data),
        _ => HttpResponse::NotFound().finish(),
    };
    Ok(response)
}

#[get("/export.csv")]
async fn export_csv(pool: web::Data<DbPool>) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_read_connection()?;
    let data = web::block(move || conn.search_items("")).await??;
    let csv = write_csv(data)?;
    Ok(HttpResponse::Ok().body(csv))
}

fn write_csv(items: Vec<ItemResult>) -> Result<Vec<u8>, CsvSerializationError> {
    let mut wtr = Writer::from_writer(vec![]);
    for item in items {
        let row = CsvRow::from_item(&item);
        wtr.serialize(row)?;
    }
    let data = wtr.into_inner()?;
    Ok(data)
}

#[post("/import_purchase")]
async fn import_purchase(
    pool: web::Data<DbPool>,
    new_purchases: Json<Vec<NewPurchase>>,
) -> Result<HttpResponse, WebError> {
    let mut conn = pool.get_write_connection()?;
    let results = web::block(move || conn.insert_purchases(&new_purchases.0)).await??;
    Ok(HttpResponse::Ok().json(results))
}

fn render_html<C>(
    tmpl: &TinyTemplate<'_>,
    template: &str,
    context: &C,
) -> Result<HttpResponse, WebError>
where
    C: Serialize,
{
    let body = tmpl.render(template, context)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}
