use actix_web::{
    body::BoxBody,
    error::{BlockingError, ResponseError},
    HttpResponse,
};
use log::error;
use thiserror::Error;

type VecIntoInnerError = csv::IntoInnerError<csv::Writer<Vec<u8>>>;

#[derive(Error, Debug)]
pub enum CsvSerializationError {
    #[error("IntoInnerError: {0}")]
    IntoInnerError(#[source] Box<VecIntoInnerError>),
    #[error(transparent)]
    CsvError(#[from] csv::Error),
}

impl From<VecIntoInnerError> for CsvSerializationError {
    fn from(inner: VecIntoInnerError) -> Self {
        Self::IntoInnerError(Box::new(inner))
    }
}

#[allow(clippy::enum_variant_names)]
#[derive(Error, Debug)]
pub enum WebError {
    #[error("DB connection pool error: {0}")]
    R2d2Error(#[from] diesel::r2d2::PoolError),
    #[error(transparent)]
    BlockingError(#[from] BlockingError),
    #[error("DB error: {0}")]
    DieselError(#[from] diesel::result::Error),
    #[error("Template error: {0}")]
    TemplateError(#[from] tinytemplate::error::Error),
    #[error("CSV serialization error: {0}")]
    CsvSerializationError(#[from] CsvSerializationError),
}

impl ResponseError for WebError {
    fn error_response(&self) -> HttpResponse<BoxBody> {
        error!("{}", self);
        HttpResponse::new(self.status_code())
    }
}
