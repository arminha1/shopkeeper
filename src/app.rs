use shopkeeper_data::{Attachment, ImportPurchaseResult, NewPurchase, PurchaseHeader};
use uuid::Uuid;

use crate::data::{ItemResult, Purchase};

pub type DbError = diesel::result::Error;
pub type DbResult<T> = Result<T, DbError>;

pub trait ReadConnection {
    fn load_purchases(&mut self) -> DbResult<Vec<PurchaseHeader>>;

    fn load_purchase(&mut self, purchase_id: Uuid) -> DbResult<Option<Purchase>>;

    fn load_last_purchase(
        &mut self,
        store_filter: Option<&str>,
    ) -> DbResult<Option<PurchaseHeader>>;

    fn load_attachment(&mut self, attachment_id: Uuid) -> DbResult<Option<Attachment>>;

    fn search_items(&mut self, name: &str) -> DbResult<Vec<ItemResult>>;
}

pub trait WriteConnection: ReadConnection {
    fn insert_purchases(
        &mut self,
        purchases: &[NewPurchase],
    ) -> DbResult<Vec<ImportPurchaseResult>>;
}
