mod models;
mod schema;
mod views;

use crate::app::{DbResult, ReadConnection, WriteConnection};
use crate::data::{
    Attachment, AttachmentHeader, ImportPurchaseResult, Item, ItemData, ItemResult, NewPurchase,
    Purchase, PurchaseData, PurchaseHeader,
};
use crate::db::models::{
    InsertAttachment, InsertItem, InsertPurchase, QueryAttachment, QueryAttachmentHeader,
    QueryItem, QueryPurchase, QueryPurchaseView,
};
use anyhow::Context;
use chrono::{DateTime, Utc};
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool, PoolError, PooledConnection};
use diesel::sqlite::SqliteConnection;
use diesel_migrations::{EmbeddedMigrations, MigrationHarness};
use log::info;
use uuid::Uuid;

const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

#[derive(Clone)]
pub struct DbPool {
    inner: Pool<ConnectionManager<SqliteConnection>>,
}

impl DbPool {
    pub fn new(database_url: String) -> Result<Self, PoolError> {
        let manager = ConnectionManager::<SqliteConnection>::new(database_url);
        let inner = Pool::builder().build(manager)?;
        Ok(Self { inner })
    }

    pub fn get_read_connection(&self) -> Result<impl ReadConnection, PoolError> {
        let conn = self.inner.get()?;
        Ok(TypedConnection { conn })
    }

    pub fn get_write_connection(&self) -> Result<impl WriteConnection, PoolError> {
        let conn = self.inner.get()?;
        Ok(TypedConnection { conn })
    }

    pub fn run_migrations(&self) -> Result<(), anyhow::Error> {
        info!("Running DB migrations");
        self.inner
            .get()?
            .run_pending_migrations(MIGRATIONS)
            .map_err(|e| anyhow::anyhow!(e))
            .context("Migrations")?;
        Ok(())
    }
}

struct TypedConnection {
    conn: PooledConnection<ConnectionManager<SqliteConnection>>,
}

impl ReadConnection for TypedConnection {
    fn load_purchases(&mut self) -> DbResult<Vec<PurchaseHeader>> {
        use crate::db::views::v_purchase::dsl;
        let s = dsl::v_purchase
            .limit(2000)
            .order(dsl::purchase_time.desc())
            .load::<QueryPurchaseView>(&mut self.conn)?;
        Ok(s.into_iter().map(|p| p.into()).collect())
    }

    fn load_purchase(&mut self, purchase_id: Uuid) -> DbResult<Option<Purchase>> {
        use crate::db::schema::purchase::dsl::*;
        purchase
            .find(purchase_id.to_string())
            .first::<QueryPurchase>(&mut self.conn)
            .optional()?
            .map(|p| self.load_purchase_details(p))
            .transpose()
    }

    fn load_last_purchase(
        &mut self,
        store_filter: Option<&str>,
    ) -> DbResult<Option<PurchaseHeader>> {
        use crate::db::views::v_purchase::dsl::*;
        let select = if let Some(store_filter) = store_filter {
            v_purchase.filter(store.eq(store_filter)).into_boxed()
        } else {
            v_purchase.into_boxed()
        };
        let result = select
            .order_by(purchase_time.desc())
            .limit(1)
            .first::<QueryPurchaseView>(&mut self.conn)
            .optional()?
            .map(|p| p.into());
        Ok(result)
    }

    fn load_attachment(&mut self, attachment_id: Uuid) -> DbResult<Option<Attachment>> {
        use crate::db::schema::attachment::dsl::*;
        let result = attachment
            .find(attachment_id.to_string())
            .first::<QueryAttachment>(&mut self.conn)
            .optional()?
            .map(|a| Attachment {
                name: a.name,
                content_type: a.content_type,
                data: a.data,
            });
        Ok(result)
    }

    fn search_items(&mut self, name: &str) -> DbResult<Vec<ItemResult>> {
        use crate::db::schema::item::dsl as item_dsl;
        use crate::db::schema::purchase::dsl as purchase_dsl;
        let name_filter = format!("%{}%", name);
        let items = item_dsl::item
            .inner_join(purchase_dsl::purchase)
            .filter(item_dsl::name.like(name_filter))
            .order((purchase_dsl::purchase_time.desc(), item_dsl::sort_key.asc()))
            .load::<(QueryItem, QueryPurchase)>(&mut self.conn)?
            .into_iter()
            .map(|i| i.into())
            .collect();
        Ok(items)
    }
}

impl TypedConnection {
    fn load_purchase_details(&mut self, qp: QueryPurchase) -> DbResult<Purchase> {
        let items = self.load_items(&qp.id)?;
        let attachments = self.load_attachment_headers(&qp.id)?;
        let total = items.iter().map(|item| item.data.price).sum();
        Ok(Purchase {
            id: qp.id.parse().unwrap(),
            data: PurchaseData {
                store: qp.store,
                location: qp.location,
                purchase_time: DateTime::from_naive_utc_and_offset(qp.purchase_time, Utc),
            },
            total,
            items,
            attachments,
        })
    }

    fn load_items(&mut self, purchase_id_value: &str) -> DbResult<Vec<Item>> {
        use crate::db::schema::item::dsl::*;
        let items = item
            .filter(purchase_id.eq(purchase_id_value))
            .order(sort_key.asc())
            .load::<QueryItem>(&mut self.conn)?
            .into_iter()
            .map(|i| i.into())
            .collect();
        Ok(items)
    }

    fn load_attachment_headers(
        &mut self,
        purchase_id_value: &str,
    ) -> DbResult<Vec<AttachmentHeader>> {
        use crate::db::schema::attachment::dsl::*;
        let attachments = attachment
            .filter(purchase_id.eq(purchase_id_value))
            .order(name.asc())
            .select((id, name, content_type))
            .load::<QueryAttachmentHeader>(&mut self.conn)?
            .into_iter()
            .map(|a| a.into())
            .collect();
        Ok(attachments)
    }
}

impl WriteConnection for TypedConnection {
    fn insert_purchases(
        &mut self,
        purchases: &[NewPurchase],
    ) -> DbResult<Vec<ImportPurchaseResult>> {
        let mut results = Vec::with_capacity(purchases.len());
        for purchase in purchases {
            let result = self.insert_purchase(purchase)?;
            results.push(result);
        }
        Ok(results)
    }
}

impl TypedConnection {
    fn insert_purchase(&mut self, new_purchase: &NewPurchase) -> DbResult<ImportPurchaseResult> {
        // TODO validate attachment content types
        use crate::db::schema::purchase::dsl::purchase;
        if self.purchase_exists(new_purchase)? {
            return Ok(ImportPurchaseResult::Duplicate);
        }
        let uid = Uuid::new_v4();
        let new_id = uid.to_string();
        let insert = InsertPurchase {
            id: &new_id,
            store: &new_purchase.data.store,
            location: &new_purchase.data.location,
            purchase_time: new_purchase.data.purchase_time.naive_utc(),
        };
        diesel::insert_into(purchase)
            .values(insert)
            .execute(&mut self.conn)?;
        for (i, new_item) in new_purchase.items.iter().enumerate() {
            self.insert_item(new_item, &new_id, i as i32)?;
        }
        for attachment in new_purchase.attachments.iter() {
            self.insert_attachment(attachment, &new_id)?;
        }
        Ok(ImportPurchaseResult::Created { id: uid })
    }

    fn purchase_exists(&mut self, new_purchase: &NewPurchase) -> DbResult<bool> {
        use crate::db::schema::purchase::dsl::*;
        let new_time = new_purchase.data.purchase_time.naive_utc();
        let existing: i64 = purchase
            .count()
            .filter(store.eq(&new_purchase.data.store))
            .filter(location.eq(&new_purchase.data.location))
            .filter(purchase_time.eq(new_time))
            .get_result(&mut self.conn)?;
        Ok(existing > 0)
    }

    fn insert_item(
        &mut self,
        new_item: &ItemData,
        purchase_id: &str,
        sort_key: i32,
    ) -> DbResult<()> {
        use crate::db::schema::item::dsl::item;
        let insert = InsertItem {
            id: &Uuid::new_v4().to_string(),
            purchase_id,
            name: &new_item.name,
            amount: new_item.amount,
            price: new_item.price.into(),
            original_price: new_item.original_price.map(|m| m.into()),
            sort_key,
        };
        diesel::insert_into(item)
            .values(insert)
            .execute(&mut self.conn)?;
        Ok(())
    }

    fn insert_attachment(
        &mut self,
        new_attachment: &Attachment,
        purchase_id: &str,
    ) -> DbResult<()> {
        use crate::db::schema::attachment::dsl::attachment;
        let insert = InsertAttachment {
            id: &Uuid::new_v4().to_string(),
            purchase_id,
            name: &new_attachment.name,
            content_type: &new_attachment.content_type,
            data: &new_attachment.data,
        };
        diesel::insert_into(attachment)
            .values(insert)
            .execute(&mut self.conn)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {

    use super::*;

    fn init_test_db() -> DbPool {
        let pool = DbPool::new(":memory:".to_owned()).expect("can connect to the DB");
        pool.run_migrations()
            .expect("migrations run wihtout errors");
        pool
    }

    #[test]
    fn can_run_migrations_on_empty_db() {
        init_test_db();
    }

    #[test]
    fn load_purchases_on_empty_db_returns_empty_vec() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let purchases = conn.load_purchases().unwrap();
        assert!(purchases.is_empty());
    }

    #[test]
    fn load_purchase_on_empty_db_returns_empty() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let result = conn.load_purchase(Uuid::new_v4()).unwrap();
        assert!(result.is_none());
    }

    #[test]
    fn load_last_purchase_on_empty_db_returns_empty() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let result = conn.load_last_purchase(None).unwrap();
        assert!(result.is_none());
    }

    #[test]
    fn load_last_purchase_with_store_on_empty_db_returns_empty() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let result = conn.load_last_purchase(Some("store")).unwrap();
        assert!(result.is_none());
    }

    #[test]
    fn load_attachment_on_empty_db_returns_empty() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let result = conn.load_attachment(Uuid::new_v4()).unwrap();
        assert!(result.is_none());
    }

    #[test]
    fn search_items_on_empty_db_returns_empty_vec() {
        let pool = init_test_db();
        let mut conn = pool.get_read_connection().unwrap();
        let items = conn.search_items("").unwrap();
        assert!(items.is_empty());
    }

    #[test]
    fn insert_purchases_with_empty_vec_returns_empty_vec() {
        let pool = init_test_db();
        let mut conn = pool.get_write_connection().unwrap();
        let result = conn.insert_purchases(&vec![]).unwrap();
        assert!(result.is_empty());
    }

    #[test]
    fn insert_purchases_on_empty_db_creates_entities() {
        let pool = init_test_db();
        let mut conn = pool.get_write_connection().unwrap();
        let data = PurchaseData {
            location: "location".to_owned(),
            store: "Coop".to_owned(),
            purchase_time: "2020-10-25T00:15:20Z".parse::<DateTime<Utc>>().unwrap(),
        };
        let items = vec![ItemData {
            name: "Brot".to_owned(),
            amount: 1.0,
            price: 2.5.into(),
            original_price: None,
        }];
        let attachment = Attachment {
            name: "hello.txt".to_owned(),
            content_type: "text/plain".to_owned(),
            data: b"Hello World".to_vec(),
        };
        let new_purchase = NewPurchase {
            data: data.clone(),
            items: items.clone(),
            attachments: vec![attachment.clone()],
        };

        let result = conn.insert_purchases(&vec![new_purchase]).unwrap();

        let id = match &result[..] {
            &[ImportPurchaseResult::Created { id }] => id,
            _ => panic!("result != Created: {:?}", &result),
        };

        let loaded = conn.load_purchase(id).unwrap();
        assert!(loaded.is_some());
        let loaded = loaded.unwrap();
        assert_eq!(loaded.id, id);
        assert_eq!(loaded.data, data);
        assert_eq!(loaded.total, 2.5.into());
        assert_eq!(
            loaded.items.into_iter().map(|d| d.data).collect::<Vec<_>>(),
            items
        );
        assert_eq!(loaded.attachments.len(), 1);
        let loaded_attachment = &loaded.attachments[0];
        assert_eq!(loaded_attachment.name, attachment.name);
        assert_eq!(loaded_attachment.content_type, attachment.content_type);

        let attachment_id = loaded_attachment.id;
        let loaded_attachment = conn.load_attachment(attachment_id).unwrap();
        assert!(loaded_attachment.is_some());
        let loaded_attachment = loaded_attachment.unwrap();
        assert_eq!(loaded_attachment, attachment);
    }
}
