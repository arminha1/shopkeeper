use super::schema::{attachment, item, purchase};

use crate::data::{AttachmentHeader, Item, ItemData, ItemResult, PurchaseData, PurchaseHeader};
use chrono::{DateTime, NaiveDateTime, Utc};

#[derive(Debug, Queryable)]
pub struct QueryPurchase {
    pub id: String,
    pub store: String,
    pub location: String,
    pub purchase_time: NaiveDateTime,
}

#[derive(Debug, Queryable)]
pub struct QueryPurchaseView {
    pub id: String,
    pub store: String,
    pub location: String,
    pub purchase_time: NaiveDateTime,
    pub total: f64,
}

impl From<QueryPurchaseView> for PurchaseHeader {
    fn from(query: QueryPurchaseView) -> Self {
        Self {
            id: query.id.parse().unwrap(),
            data: PurchaseData {
                store: query.store,
                location: query.location,
                purchase_time: DateTime::from_naive_utc_and_offset(query.purchase_time, Utc),
            },
            total: query.total.into(),
        }
    }
}

#[derive(Debug, Insertable)]
#[diesel(table_name = purchase)]
pub struct InsertPurchase<'a> {
    pub id: &'a str,
    pub store: &'a str,
    pub location: &'a str,
    pub purchase_time: NaiveDateTime,
}

#[derive(Debug, Queryable)]
pub struct QueryItem {
    pub id: String,
    pub _purchase_id: String,
    pub name: String,
    pub amount: f64,
    pub price: f64,
    pub original_price: Option<f64>,
    pub _sort_key: i32,
}

impl From<QueryItem> for Item {
    fn from(query: QueryItem) -> Self {
        Self {
            id: query.id.parse().unwrap(),
            data: ItemData {
                name: query.name,
                amount: query.amount,
                price: query.price.into(),
                original_price: query.original_price.map(|f| f.into()),
            },
        }
    }
}

impl From<(QueryItem, QueryPurchase)> for ItemResult {
    fn from((item, purchase): (QueryItem, QueryPurchase)) -> Self {
        Self {
            id: item.id.parse().unwrap(),
            data: ItemData {
                name: item.name,
                amount: item.amount,
                price: item.price.into(),
                original_price: item.original_price.map(|f| f.into()),
            },
            purchase_id: purchase.id.parse().unwrap(),
            purchase: PurchaseData {
                store: purchase.store,
                location: purchase.location,
                purchase_time: DateTime::from_naive_utc_and_offset(purchase.purchase_time, Utc),
            },
        }
    }
}

#[derive(Debug, Insertable)]
#[diesel(table_name = item)]
pub struct InsertItem<'a> {
    pub id: &'a str,
    pub purchase_id: &'a str,
    pub name: &'a str,
    pub amount: f64,
    pub price: f64,
    pub original_price: Option<f64>,
    pub sort_key: i32,
}

#[derive(Debug, Queryable)]
pub struct QueryAttachment {
    pub _id: String,
    pub _purchase_id: String,
    pub name: String,
    pub content_type: String,
    pub data: Vec<u8>,
}

#[derive(Debug, Queryable)]
pub struct QueryAttachmentHeader {
    pub id: String,
    pub name: String,
    pub content_type: String,
}

impl From<QueryAttachmentHeader> for AttachmentHeader {
    fn from(query: QueryAttachmentHeader) -> Self {
        AttachmentHeader {
            id: query.id.parse().unwrap(),
            name: query.name,
            content_type: query.content_type,
        }
    }
}

#[derive(Debug, Insertable)]
#[diesel(table_name = attachment)]
pub struct InsertAttachment<'a> {
    pub id: &'a str,
    pub purchase_id: &'a str,
    pub name: &'a str,
    pub content_type: &'a str,
    pub data: &'a [u8],
}
