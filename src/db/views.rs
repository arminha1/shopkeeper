table! {
    v_purchase (id) {
        id -> Text,
        store -> Text,
        location -> Text,
        purchase_time -> Timestamp,
        total -> Double,
    }
}
