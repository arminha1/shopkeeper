// @generated automatically by Diesel CLI.

diesel::table! {
    attachment (id) {
        id -> Text,
        purchase_id -> Text,
        name -> Text,
        content_type -> Text,
        data -> Binary,
    }
}

diesel::table! {
    item (id) {
        id -> Text,
        purchase_id -> Text,
        name -> Text,
        amount -> Double,
        price -> Double,
        original_price -> Nullable<Double>,
        sort_key -> Integer,
    }
}

diesel::table! {
    purchase (id) {
        id -> Text,
        store -> Text,
        location -> Text,
        purchase_time -> Timestamp,
    }
}

diesel::joinable!(attachment -> purchase (purchase_id));
diesel::joinable!(item -> purchase (purchase_id));

diesel::allow_tables_to_appear_in_same_query!(attachment, item, purchase,);
