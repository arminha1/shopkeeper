use actix_web::{get, HttpResponse, Responder};

const FAVICON_ICO: &[u8] = include_bytes!("assets/favicon/favicon.ico");
const FAVICON_16: &[u8] = include_bytes!("assets/favicon/favicon-16x16.png");
const FAVICON_32: &[u8] = include_bytes!("assets/favicon/favicon-32x32.png");
const APPLE_TOUCH_ICON: &[u8] = include_bytes!("assets/favicon/apple-touch-icon.png");
const SAFARI_PINNED_TAB: &[u8] = include_bytes!("assets/favicon/safari-pinned-tab.svg");
const SITE_WEBMANIFEST: &str = include_str!("assets/favicon/site.webmanifest");
const ANDROID_CHROME_192: &[u8] = include_bytes!("assets/favicon/android-chrome-192x192.png");
const ANDROID_CHROME_512: &[u8] = include_bytes!("assets/favicon/android-chrome-512x512.png");

const IMAGE_PNG: &str = "image/png";

#[get("/favicon.ico")]
pub async fn favicon() -> impl Responder {
    HttpResponse::Ok()
        .content_type("image/x-icon")
        .body(FAVICON_ICO)
}

#[get("/favicon-16x16.png")]
pub async fn favicon16() -> impl Responder {
    HttpResponse::Ok().content_type(IMAGE_PNG).body(FAVICON_16)
}

#[get("/favicon-32x32.png")]
pub async fn favicon32() -> impl Responder {
    HttpResponse::Ok().content_type(IMAGE_PNG).body(FAVICON_32)
}

#[get("/apple-touch-icon.png")]
pub async fn apple_touch_icon() -> impl Responder {
    HttpResponse::Ok()
        .content_type(IMAGE_PNG)
        .body(APPLE_TOUCH_ICON)
}

#[get("/safari-pinned-tab.svg")]
pub async fn safari_pinned_tab() -> impl Responder {
    HttpResponse::Ok()
        .content_type("image/svg+xml")
        .body(SAFARI_PINNED_TAB)
}

#[get("/site.webmanifest")]
pub async fn site_webmanifest() -> impl Responder {
    HttpResponse::Ok()
        .content_type("application/manifest+json")
        .body(SITE_WEBMANIFEST)
}

#[get("/android-chrome-192x192.png")]
pub async fn android_chrome() -> impl Responder {
    HttpResponse::Ok()
        .content_type(IMAGE_PNG)
        .body(ANDROID_CHROME_192)
}

#[get("/android-chrome-512x512.png")]
pub async fn android_chrome_large() -> impl Responder {
    HttpResponse::Ok()
        .content_type(IMAGE_PNG)
        .body(ANDROID_CHROME_512)
}
