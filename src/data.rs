use chrono::{DateTime, Datelike, Utc};
use serde::Serialize;
pub use shopkeeper_data::*;
use uuid::Uuid;

#[derive(Debug, Serialize, PartialEq)]
pub struct Purchase {
    pub id: Uuid,
    #[serde(flatten)]
    pub data: PurchaseData,
    pub total: Money,
    pub items: Vec<Item>,
    pub attachments: Vec<AttachmentHeader>,
}

#[derive(Debug, Serialize, PartialEq)]
pub struct Item {
    pub id: Uuid,
    #[serde(flatten)]
    pub data: ItemData,
}

#[derive(Debug, Serialize)]
pub struct ItemResult {
    pub id: Uuid,
    #[serde(flatten)]
    pub data: ItemData,
    pub purchase_id: Uuid,
    #[serde(flatten)]
    pub purchase: PurchaseData,
}

#[derive(Debug, Serialize)]
pub struct CsvRow<'a> {
    #[serde(rename = "Name")]
    name: &'a str,
    #[serde(rename = "Amount")]
    amount: f64,
    #[serde(rename = "Price")]
    price: Money,
    #[serde(rename = "Original Price")]
    original_price: Option<Money>,
    #[serde(rename = "Store")]
    store: &'a str,
    #[serde(rename = "Location")]
    location: &'a str,
    #[serde(rename = "Time")]
    time: DateTime<Utc>,
}

impl<'a> CsvRow<'a> {
    pub fn from_item(item: &'a ItemResult) -> Self {
        Self {
            name: &item.data.name,
            amount: item.data.amount,
            price: item.data.price,
            original_price: item.data.original_price,
            store: &item.purchase.store,
            location: &item.purchase.location,
            time: item.purchase.purchase_time,
        }
    }
}

#[derive(Debug, Serialize, PartialEq, PartialOrd, Eq, Ord)]
pub struct YearMonth {
    pub year: i32,
    pub month: u32,
}

impl YearMonth {
    pub fn from_date<D: Datelike>(date: D) -> Self {
        Self {
            year: date.year(),
            month: date.month(),
        }
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct MonthlySpending {
    pub month: YearMonth,
    pub total: Money,
}

impl MonthlySpending {
    pub fn new(month: YearMonth, total: Money) -> Self {
        Self { month, total }
    }
}
