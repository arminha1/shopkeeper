#!/bin/bash
set -e

SCRIPT_DIR=$(dirname $0)

rm -rf build
mkdir build

rsync -av --exclude-from=$SCRIPT_DIR/.gitignore \
      --exclude=.git --exclude=build --exclude=apt --exclude=cargo \
      $SCRIPT_DIR/ build/shopkeeper

pushd build
pushd shopkeeper

# create changelog
AUTHOR="Armin Häberling <armin.aha@gmail.com>"
VERSION=`cargo metadata --format-version 1 --no-deps | python3 -c "import sys, json; print(json.load(sys.stdin)['packages'][0]['version'])"`

cat > debian/changelog << EOF
shopkeeper ($VERSION) unstable; urgency=low

  * Packaged ${VERSION}

 -- ${AUTHOR}  $(date -R)

EOF

dpkg-buildpackage -b -rfakeroot -us -uc
popd
lintian --suppress-tags possible-gpl-code-linked-with-openssl shopkeeper_*.changes
popd
