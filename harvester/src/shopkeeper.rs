use reqwest::{IntoUrl, Url};
use shopkeeper_data::{ImportPurchaseResult, NewPurchase};

pub struct ShopkeeperClient {
    http_client: reqwest::Client,
    base_url: Url,
}

impl ShopkeeperClient {
    pub fn new<U>(base_url: U) -> reqwest::Result<Self>
    where
        U: IntoUrl,
    {
        let mut base_url = base_url.into_url()?;
        if !base_url.path().ends_with('/') {
            base_url.set_path(&format!("{}/", base_url.path()));
        }
        let http_client = reqwest::Client::new();
        Ok(Self {
            http_client,
            base_url,
        })
    }

    pub async fn import_purchase(
        &self,
        purchase: NewPurchase,
    ) -> anyhow::Result<ImportPurchaseResult> {
        let url = self.base_url.join("import_purchase")?;
        let purchases = vec![purchase];
        let resp = self.http_client.post(url).json(&purchases).send().await?;
        let mut res = resp.json::<Vec<ImportPurchaseResult>>().await?;
        Ok(res.swap_remove(0))
    }
}
