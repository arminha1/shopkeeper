#![forbid(unsafe_code)]

mod coop;
mod migros;
mod shopkeeper;

use chrono::{Datelike, Duration, Local, NaiveDate, ParseError};
use clap::Parser;
use std::path::PathBuf;

fn parse_date(src: &str) -> Result<NaiveDate, ParseError> {
    NaiveDate::parse_from_str(src, "%F")
}

#[derive(Debug, Parser)]
#[clap(version)]
enum Opt {
    #[command(subcommand)]
    Migros(MigrosOpt),
    #[command(subcommand)]
    Coop(CoopOpt),
}

/// Commands for Migros
#[derive(Debug, Parser, Clone)]
enum MigrosOpt {
    /// List receipts from the Migros website that would be processed
    List {
        /// Cookie value from a valid login
        cookie_value: String,
        /// Date to import from. Minimum is 2 years ago. Default is 4 weeks before <to>
        #[clap(long, value_parser = parse_date)]
        from: Option<NaiveDate>,
        /// Date to import to. Maximum is today. Default is today
        #[clap(long, value_parser = parse_date)]
        to: Option<NaiveDate>,
    },
    /// Import receipts from the Migros website
    Import {
        /// Cookie value from a valid login
        cookie_value: String,
        /// Date to import from. Minimum is 2 years ago. Default is 4 weeks before <to>
        #[clap(long, value_parser = parse_date)]
        from: Option<NaiveDate>,
        /// Date to import to. Maximum is today. Default is today
        #[clap(long, value_parser = parse_date)]
        to: Option<NaiveDate>,
        /// shopkeeper server URL
        server: String,
    },
    /// Parse a CSV file receipt and print the content to stdout
    Parse { csv_file: PathBuf },
}

/// Commands for Coop
#[derive(Debug, Parser)]
enum CoopOpt {
    /// List receipts from the Coop website that would be processed
    List {
        /// Cookie value from a valid login
        cookie_value: String,
        /// Date to import from. Minimum is 2 years ago. Default is 4 weeks before <to>
        #[clap(long, value_parser = parse_date)]
        from: Option<NaiveDate>,
        /// Date to import to. Maximum is today. Default is today
        #[clap(long, value_parser = parse_date)]
        to: Option<NaiveDate>,
    },
    /// Import receipts from the Coop website
    Import {
        /// Cookie value from a valid login
        cookie_value: String,
        /// Date to import from. Minimum is 2 years ago. Default is 4 weeks before <to>
        #[clap(long, value_parser = parse_date)]
        from: Option<NaiveDate>,
        /// Date to import to. Maximum is today. Default is today
        #[clap(long, value_parser = parse_date)]
        to: Option<NaiveDate>,
        /// shopkeeper server URL
        server: String,
    },
    /// Parse a PDF file receipt and print the content to stdout
    Parse { pdf_file: PathBuf },
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let opt = Opt::parse();
    env_logger::init();

    match opt {
        Opt::Migros(opt) => {
            migros_cmd(opt).await?;
        }
        Opt::Coop(opt) => {
            coop_cmd(opt).await?;
        }
    }

    Ok(())
}

async fn migros_cmd(opt: MigrosOpt) -> anyhow::Result<()> {
    match opt {
        MigrosOpt::List {
            cookie_value,
            from,
            to,
        } => {
            let (from, to) = validate_dates(from, to);
            migros::list(cookie_value, from, to).await?;
        }
        MigrosOpt::Import {
            cookie_value,
            from,
            to,
            server,
        } => {
            let (from, to) = validate_dates(from, to);
            migros::import(cookie_value, from, to, &server).await?;
        }
        MigrosOpt::Parse { csv_file } => {
            migros::parse(&csv_file)?;
        }
    }

    Ok(())
}

async fn coop_cmd(opt: CoopOpt) -> anyhow::Result<()> {
    match opt {
        CoopOpt::List {
            cookie_value,
            from,
            to,
        } => {
            let (from, to) = validate_dates(from, to);
            coop::list(cookie_value, from, to).await?;
        }
        CoopOpt::Import {
            cookie_value,
            from,
            to,
            server,
        } => {
            let (from, to) = validate_dates(from, to);
            coop::import(cookie_value, from, to, &server).await?;
        }
        CoopOpt::Parse { pdf_file } => {
            coop::parse(&pdf_file)?;
        }
    }

    Ok(())
}

pub fn validate_dates(from: Option<NaiveDate>, to: Option<NaiveDate>) -> (NaiveDate, NaiveDate) {
    let today = Local::now().date_naive();
    let min = today.with_year(today.year() - 2).unwrap();
    let to = match to {
        Some(to) => {
            if to > today {
                today
            } else {
                to
            }
        }
        None => today,
    };
    let from = match from {
        Some(from) => from.clamp(min, to),
        None => to - Duration::weeks(4),
    };
    (from, to)
}

#[test]
fn verify_app() {
    use clap::CommandFactory;
    Opt::command().debug_assert()
}
