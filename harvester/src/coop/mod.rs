use crate::shopkeeper::ShopkeeperClient;
use bytes::Bytes;
use chrono::{FixedOffset, NaiveDate};
use log::debug;
use reqwest::header::{COOKIE, USER_AGENT};
use reqwest::Client;
use serde::Deserialize;
use shopkeeper_data::{Attachment, Money, NewPurchase};
use std::fs::File;

#[allow(unused)]
#[derive(Debug, Deserialize)]
struct GetPurchasesResponse {
    pagination: Pagination,
    purchases: Vec<PurchaseHeader>,
}

#[allow(unused)]
#[derive(Debug, Deserialize)]
struct Pagination {
    #[serde(rename = "pageSize")]
    page_size: u32,
    #[serde(rename = "currentPage")]
    current_page: u32,
}

#[allow(unused)]
#[derive(Debug, Deserialize)]
struct PurchaseHeader {
    #[serde(rename = "encBarcode")]
    enc_barcode: EncodedBarcode,
    barcode: String,
    date: chrono::DateTime<FixedOffset>,
    total: Total,
    location: Location,
}

#[allow(unused)]
#[derive(Debug, Deserialize)]

struct Total {
    amount: u32,
    currency: String,
}

#[derive(Debug, Deserialize)]
struct EncodedBarcode(String);

#[allow(unused)]
#[derive(Debug, Deserialize)]
struct Location {
    name: String,
}

const ISO_DATE_FORMAT: &str = "%Y-%m-%d";
const GET_PURCHASES_URL: &str =
    "https://www.supercard.ch/bin/coop/supercard/digitalReceipt/getPurchases.json";
const PDF_URL: &str = "https://www.supercard.ch/bin/coop/kbk/kassenzettelpoc";
const USER_AGENT_VALUE: &str =
    "Mozilla/5.0 (X11; Linux x86_64; rv:134.0) Gecko/20100101 Firefox/134.0";

struct CoopClient {
    client: Client,
    cookie_value: String,
}

impl CoopClient {
    fn new(cookie_value: String) -> Self {
        Self {
            client: Client::new(),
            cookie_value,
        }
    }

    async fn fetch_purchase_headers(
        &self,
        page: u32,
        period: Option<(NaiveDate, NaiveDate)>,
    ) -> Result<GetPurchasesResponse, anyhow::Error> {
        let mut query = vec![("currentPage", page.to_string())];
        if let Some((from, to)) = period {
            query.push(("dateMin", from.format(ISO_DATE_FORMAT).to_string()));
            query.push(("dateMax", to.format(ISO_DATE_FORMAT).to_string()));
        }
        let res = self
            .client
            .get(GET_PURCHASES_URL)
            .query(&query)
            .header(COOKIE, &self.cookie_value)
            .header(USER_AGENT, USER_AGENT_VALUE)
            .send()
            .await?;
        let body: GetPurchasesResponse = res.json().await?;
        Ok(body)
    }

    async fn fetch_all_purchase_headers(
        &self,
        period: Option<(NaiveDate, NaiveDate)>,
    ) -> Result<Vec<PurchaseHeader>, anyhow::Error> {
        let page = 2;
        let response = self.fetch_purchase_headers(page, period).await?;
        Ok(response.purchases)
    }

    async fn fetch_pdf(&self, enc_barcode: &EncodedBarcode) -> Result<Bytes, reqwest::Error> {
        let query = [("bc", enc_barcode.0.as_str()), ("pdfType", "receipt")];
        let res = self
            .client
            .get(PDF_URL)
            .query(&query)
            .header(COOKIE, &self.cookie_value)
            .header(USER_AGENT, USER_AGENT_VALUE)
            .send()
            .await?;
        let body = res.bytes().await?;
        Ok(body)
    }
}

pub async fn list(cookie_value: String, from: NaiveDate, to: NaiveDate) -> anyhow::Result<()> {
    let client = CoopClient::new(cookie_value);
    println!("Fetch receipts from {} to {}", from, to);
    let purchase_headers = client.fetch_all_purchase_headers(Some((from, to))).await?;
    debug!("{:#?}", purchase_headers);
    println!("Found {} receipts", purchase_headers.len());

    for header in purchase_headers {
        let purchase = fetch_purchase(&client, &header).await?;
        let total: Money = purchase.items.iter().map(|i| i.price).sum();
        println!(
            "{} @ {} - Total {}",
            purchase.data.purchase_time, purchase.data.location, total
        );
    }
    Ok(())
}

pub async fn import(
    cookie_value: String,
    from: NaiveDate,
    to: NaiveDate,
    server: &str,
) -> anyhow::Result<()> {
    let client = CoopClient::new(cookie_value);
    println!("Fetch receipts from {} to {}", from, to);
    let purchase_headers = client.fetch_all_purchase_headers(Some((from, to))).await?;
    debug!("{:#?}", purchase_headers);
    println!("Found {} receipts", purchase_headers.len());

    let sk_client = ShopkeeperClient::new(server)?;
    for header in purchase_headers {
        let purchase = fetch_purchase(&client, &header).await?;
        let total: Money = purchase.items.iter().map(|i| i.price).sum();
        println!(
            "{} @ {} - Total {}",
            purchase.data.purchase_time, purchase.data.location, total
        );

        let res = sk_client.import_purchase(purchase).await?;
        println!("Message {} {:?}", &header.barcode, res);
    }
    Ok(())
}

async fn fetch_purchase(
    client: &CoopClient,
    header: &PurchaseHeader,
) -> anyhow::Result<NewPurchase> {
    let pdf = client.fetch_pdf(&header.enc_barcode).await?;
    let mut purchase = coop_receipt_parser::parse_pdf_from_data(&pdf)?;
    let attachment = Attachment {
        content_type: "application/pdf".to_owned(),
        data: pdf.to_vec(),
        name: format!("{}.pdf", header.barcode),
    };
    purchase.attachments.push(attachment);
    Ok(purchase)
}

pub fn parse(pdf_file: &std::path::PathBuf) -> anyhow::Result<()> {
    let mut file = File::open(pdf_file)?;
    let purchase = coop_receipt_parser::parse_pdf(&mut file)?;
    println!("{:#?}", purchase);
    Ok(())
}

#[cfg(test)]

mod tests {

    use super::*;
    use chrono::TimeZone;

    #[test]
    fn parse_get_purchase_response() {
        let json = include_str!("example_get_purchase_response.json");
        let response: GetPurchasesResponse = serde_json::from_str(json).expect("Parse error");
        assert_eq!(response.purchases.len(), 2);
        let purchase = &response.purchases[0];
        assert_eq!(
            purchase.enc_barcode.0,
            "FHaFY_wgX9q-VfdZX4GxDoN4kk1agJCjoa_l0m7yBWUXMZ1VvVTpew"
        );
        assert_eq!(purchase.barcode, "9900200192214052200063702982");
        assert_eq!(
            purchase.date,
            FixedOffset::east_opt(0)
                .unwrap()
                .with_ymd_and_hms(2022, 5, 14, 16, 4, 41)
                .unwrap()
        );
        assert_eq!(purchase.location.name, "Wohlen");
    }
}
