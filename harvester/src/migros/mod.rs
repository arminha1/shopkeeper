pub mod client;

use crate::migros::client::MigrosClient;
use crate::shopkeeper::ShopkeeperClient;
use anyhow::bail;
use chrono::NaiveDate;
use shopkeeper_data::{Attachment, Money, NewPurchase};
use std::fs::File;
use std::path::Path;

pub async fn list(cookie_value: String, from: NaiveDate, to: NaiveDate) -> anyhow::Result<()> {
    let client = MigrosClient::new(cookie_value);
    let ids = fetch_ids(&client, from, to).await?;

    println!("Found {} receipts", ids.len());

    for id in ids {
        if let Some(purchase) = fetch_purchase(&client, &id, false).await? {
            let total: Money = purchase.items.iter().map(|i| i.price).sum();
            println!(
                "{} @ {} - Total {}",
                purchase.data.purchase_time, purchase.data.location, total
            );
        }
    }

    Ok(())
}

pub async fn import(
    cookie_value: String,
    from: NaiveDate,
    to: NaiveDate,
    server: &str,
) -> anyhow::Result<()> {
    let client = MigrosClient::new(cookie_value);
    let ids = fetch_ids(&client, from, to).await?;

    println!("Found {} receipts", ids.len());

    let sk_client = ShopkeeperClient::new(server)?;

    for id in ids {
        if let Some(purchase) = fetch_purchase(&client, &id, true).await? {
            let total: Money = purchase.items.iter().map(|i| i.price).sum();
            println!(
                "{} @ {} - Total {}",
                purchase.data.purchase_time, purchase.data.location, total
            );

            let res = sk_client.import_purchase(purchase).await?;
            println!("Message {} {:?}", &id, res);
        }
    }

    Ok(())
}

async fn fetch_ids(
    client: &MigrosClient,
    from: NaiveDate,
    to: NaiveDate,
) -> anyhow::Result<Vec<String>> {
    println!("Fetch receipts from {} to {}", from, to);
    let period = Some((from, to));
    let ids = client.fetch_all_ids(period).await?;
    Ok(ids)
}

async fn fetch_purchase(
    client: &MigrosClient,
    id: &str,
    fetch_pdf: bool,
) -> anyhow::Result<Option<NewPurchase>> {
    let csv = client.fetch_csv(id).await?;
    let mut purchases = migros_receipt_parser::parse_csv(csv.as_ref())?;
    let mut purchase = match purchases.len() {
        1 => purchases.pop().unwrap(),
        0 => return Ok(None),
        n => bail!("should be zero or one result but was {}", n),
    };
    if fetch_pdf {
        let pdf = client.fetch_pdf(id).await?;
        let attachment = Attachment {
            content_type: "application/pdf".to_owned(),
            data: pdf.to_vec(),
            name: format!("{}.pdf", id),
        };
        purchase.attachments.push(attachment);
    }
    let attachment = Attachment {
        content_type: "text/csv".to_owned(),
        data: csv.to_vec(),
        name: format!("{}.csv", id),
    };
    purchase.attachments.push(attachment);
    Ok(Some(purchase))
}

pub fn parse(csv_file: &Path) -> anyhow::Result<()> {
    let file = File::open(csv_file)?;
    let purchases = migros_receipt_parser::parse_csv(file)?;
    println!("{:#?}", purchases);
    Ok(())
}
