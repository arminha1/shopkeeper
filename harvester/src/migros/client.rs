use bytes::Bytes;
use chrono::NaiveDate;
use reqwest::header::COOKIE;
use reqwest::Client;
use scraper::{Html, Selector};

const ISO_DATE_FORMAT: &str = "%Y-%m-%d";

const LIST_IDS_URL: &str = "https://cumulus.migros.ch/de/konto/kassenbons/variants/variant-1/content/04/ajaxContent/0.html";
const PDF_URL: &str = "https://cumulus.migros.ch/service/avantaReceiptExport/pdf";
const CSV_URL: &str = "https://cumulus.migros.ch/service/avantaReceiptExport/csv.csv";

pub struct MigrosClient {
    client: Client,
    cookie_value: String,
}

impl MigrosClient {
    pub fn new(cookie_value: String) -> Self {
        let client = Client::new();
        Self {
            client,
            cookie_value,
        }
    }

    pub async fn fetch_ids(
        &self,
        page: u32,
        period: Option<(NaiveDate, NaiveDate)>,
    ) -> Result<Vec<String>, reqwest::Error> {
        let mut query = vec![("p", page.to_string())];
        if let Some((from, to)) = period {
            query.push((
                "period",
                format!(
                    "{}_{}",
                    from.format(ISO_DATE_FORMAT),
                    to.format(ISO_DATE_FORMAT)
                ),
            ));
        }
        let res = self
            .client
            .get(LIST_IDS_URL)
            .query(&query)
            .header(COOKIE, &self.cookie_value)
            .send()
            .await?;
        let body = res.text().await?;

        let fragment = Html::parse_document(&body);
        let table_ids = Selector::parse("table > tbody > tr > th > label > input").unwrap();
        let items: Vec<_> = fragment
            .select(&table_ids)
            .filter_map(|elem| elem.value().attr("value"))
            .map(str::to_string)
            .collect();
        Ok(items)
    }

    pub async fn fetch_all_ids(
        &self,
        period: Option<(NaiveDate, NaiveDate)>,
    ) -> Result<Vec<String>, reqwest::Error> {
        let mut page = 1;
        let mut ids: Vec<String> = Vec::new();
        loop {
            let mut ids_of_page = self.fetch_ids(page, period).await?;
            if ids_of_page.is_empty() {
                break;
            }
            ids.append(&mut ids_of_page);
            page += 1;
        }
        Ok(ids)
    }

    pub async fn fetch_pdf(&self, item_id: &str) -> Result<Bytes, reqwest::Error> {
        let query = [("receiptId", item_id), ("fallbackLanguage", "de")];
        let res = self
            .client
            .get(PDF_URL)
            .query(&query)
            .header(COOKIE, &self.cookie_value)
            .send()
            .await?;
        let body = res.bytes().await?;
        Ok(body)
    }

    pub async fn fetch_csv(&self, item_id: &str) -> Result<Bytes, reqwest::Error> {
        let params = [
            ("sort", "dateDsc"),
            ("language", "de"),
            ("details", "true"),
            ("checkbox1", item_id),
        ];
        let res = self
            .client
            .post(CSV_URL)
            .header(COOKIE, &self.cookie_value)
            .form(&params)
            .send()
            .await?;
        let body = res.bytes().await?;
        Ok(body)
    }
}
