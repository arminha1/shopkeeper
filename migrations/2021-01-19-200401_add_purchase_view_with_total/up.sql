CREATE VIEW v_purchase
AS
SELECT
  p.id,
  p.store,
  p.location,
  p.purchase_time,
  (SELECT sum(i.price) FROM item i WHERE i.purchase_id = p.id) as total
FROM purchase p;
