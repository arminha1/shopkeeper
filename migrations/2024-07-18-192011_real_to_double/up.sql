CREATE TABLE item_new (
  id VARCHAR NOT NULL PRIMARY KEY,
  purchase_id VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  amount DOUBLE NOT NULL,
  price DOUBLE NOT NULL,
  original_price DOUBLE,
  sort_key INTEGER NOT NULL,
  UNIQUE (purchase_id, sort_key),
  FOREIGN KEY(purchase_id) REFERENCES purchase(id)
);
INSERT INTO item_new SELECT * FROM item;

DROP VIEW v_purchase;
DROP TABLE item;

ALTER TABLE item_new RENAME TO item;

CREATE VIEW v_purchase
AS
SELECT
  p.id,
  p.store,
  p.location,
  p.purchase_time,
  (SELECT sum(i.price) FROM item i WHERE i.purchase_id = p.id) as total
FROM purchase p;
