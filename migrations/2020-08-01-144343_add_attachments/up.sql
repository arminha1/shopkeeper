CREATE TABLE attachment (
  id VARCHAR NOT NULL PRIMARY KEY,
  purchase_id VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  content_type VARCHAR NOT NULL,
  data BLOB NOT NULL,
  FOREIGN KEY(purchase_id) REFERENCES purchase(id)
);
