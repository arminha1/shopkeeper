CREATE TABLE purchase (
  id VARCHAR NOT NULL PRIMARY KEY,
  store VARCHAR NOT NULL,
  location VARCHAR NOT NULL,
  purchase_time TIMESTAMP NOT NULL,
  UNIQUE (store, location, purchase_time)
);

CREATE TABLE item (
  id VARCHAR NOT NULL PRIMARY KEY,
  purchase_id VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  amount REAL NOT NULL,
  price REAL NOT NULL,
  original_price REAL,
  sort_key INTEGER NOT NULL,
  UNIQUE (purchase_id, sort_key),
  FOREIGN KEY(purchase_id) REFERENCES purchase(id)
);
