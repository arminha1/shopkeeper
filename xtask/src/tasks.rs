use crate::ops::target_dir;
use clap::{Parser, ValueEnum};
use xshell::{cmd, Shell};

/// Build all packages
#[derive(Debug, Parser)]
pub struct BuildCmd;

impl BuildCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        cmd!(sh, "cargo build --all").run()?;
        Ok(())
    }
}

/// Run Clippy for all packages
#[derive(Debug, Parser)]
pub struct ClippyCmd;

impl ClippyCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        cmd!(sh, "cargo clippy --workspace -- -D warnings").run()?;
        Ok(())
    }
}

/// Run all tests
#[derive(Debug, Parser)]
pub struct TestCmd;

impl TestCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        cmd!(sh, "cargo test --all").run()?;
        Ok(())
    }
}

/// Format code or check formatting
#[derive(Debug, Parser)]
pub struct FormatCmd {
    #[arg(short, long)]
    check: bool,
}

impl FormatCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        if self.check {
            cmd!(sh, "cargo fmt --all -- --check").run()?;
        } else {
            cmd!(sh, "cargo fmt --all").run()?;
        }
        Ok(())
    }
}

/// Run all checks that run in CI
#[derive(Debug, Parser)]
pub struct CiCmd;

impl CiCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        FormatCmd { check: true }.run(sh)?;
        ClippyCmd.run(sh)?;
        TestCmd.run(sh)?;
        Ok(())
    }
}

/// Measure test coverage
#[derive(Debug, Parser)]
pub struct CoverageCmd {
    /// Coverage report format
    #[arg(short, long, value_enum, default_value_t = CoverageReportFormat::Html)]
    format: CoverageReportFormat,
}

#[derive(Debug, Clone, Copy, ValueEnum)]
pub enum CoverageReportFormat {
    Html,
    Lcov,
    Cobertura,
}

impl CoverageCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        let output_dir = target_dir().join("coverage");
        sh.remove_path(&output_dir)?;
        sh.create_dir(&output_dir)?;

        println!("=== running coverage ===");
        let raw_dir = output_dir.join("raw");
        sh.create_dir(&raw_dir)?;
        let abs_raw = raw_dir.canonicalize()?;
        let abs_raw = abs_raw.to_string_lossy().into_owned();
        cmd!(sh, "cargo test --all")
            .env("CARGO_INCREMENTAL", "0")
            .env("RUSTFLAGS", "-Cinstrument-coverage")
            .env(
                "LLVM_PROFILE_FILE",
                format!("{abs_raw}/cargo-test-%p-%m.profraw"),
            )
            .run()?;
        println!("ok.");

        println!("=== generating report ===");
        let (fmt, file) = match self.format {
            CoverageReportFormat::Html => ("html", "target/coverage/html"),
            CoverageReportFormat::Lcov => ("lcov", "target/coverage/tests.lcov"),
            CoverageReportFormat::Cobertura => ("cobertura", "target/coverage/cobertura.xml"),
        };

        cmd!(
            sh,
            "grcov {abs_raw}
                --binary-path ./target/debug/deps
                -s .
                -t {fmt}
                --branch
                --ignore-not-existing
                --ignore target/*
                --ignore xtask/*
                --ignore */src/tests/*
                --ignore */.cargo/*
                -o {file}"
        )
        .run()?;
        println!("ok.");

        println!("report location: {}", file);

        Ok(())
    }
}

/// Build ocumentation
#[derive(Debug, Parser)]
pub struct DocCmd;

impl DocCmd {
    pub fn run(&self, sh: &Shell) -> anyhow::Result<()> {
        cmd!(sh, "cargo doc --workspace").run()?;
        Ok(())
    }
}
