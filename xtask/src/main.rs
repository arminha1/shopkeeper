use clap::Parser;
use ops::project_root;
use xshell::Shell;

mod ops;
mod tasks;

#[derive(Debug, Parser)]
enum Args {
    Build(tasks::BuildCmd),
    Clippy(tasks::ClippyCmd),
    Test(tasks::TestCmd),
    Fmt(tasks::FormatCmd),
    Coverage(tasks::CoverageCmd),
    Ci(tasks::CiCmd),
    Doc(tasks::DocCmd),
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    let sh = &Shell::new()?;
    sh.change_dir(project_root());

    match args {
        Args::Build(cmd) => cmd.run(sh),
        Args::Clippy(cmd) => cmd.run(sh),
        Args::Test(cmd) => cmd.run(sh),
        Args::Fmt(cmd) => cmd.run(sh),
        Args::Coverage(cmd) => cmd.run(sh),
        Args::Ci(cmd) => cmd.run(sh),
        Args::Doc(cmd) => cmd.run(sh),
    }
}
