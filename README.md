# shopkeeper

[![dependency status](https://deps.rs/repo/gitlab/arminha1/shopkeeper/status.svg)](https://deps.rs/repo/gitlab/arminha1/shopkeeper)

Analyze your grocery shopping spending

## Release How-To

The following command bumps the version and creates a release commit and the corresponding git tag.

```sh
./make_release.sh [major/minor/patch]
```
