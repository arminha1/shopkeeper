# Favicons

## Source

![groceries color](groceries_color.svg)

[Groceries Color](https://www.svgrepo.com/svg/90104/groceries)

![groceries monochrome](groceries_monochrome.svg)

[Groceries Monochrome](https://www.svgrepo.com/svg/3868/groceries)

## Generator

The favicons were generated with [RealFaviconGenerator](https://realfavicongenerator.net/).