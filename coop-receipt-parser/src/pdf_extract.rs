use anyhow::{bail, ensure, Context};
use log::{debug, trace};
use pdf::content::Op;
use pdf::encoding::BaseEncoding;
use pdf::file::FileOptions;
use pdf::font::Font;
use pdf::object::{MaybeRef, Page, Resolve};
use pdf::primitive::PdfString;
use std::borrow::Cow;
use std::collections::HashMap;

struct FontCache {
    fonts: HashMap<String, MaybeRef<Font>>,
}

impl FontCache {
    fn new() -> Self {
        Self {
            fonts: HashMap::new(),
        }
    }

    fn add_font(&mut self, name: String, font: MaybeRef<Font>) {
        debug!("Add font {}: {:?}", name, font);
        self.fonts.insert(name, font);
    }

    fn get_font(&self, name: &str) -> Option<&MaybeRef<Font>> {
        self.fonts.get(name)
    }
}

pub fn extract_text(pdf_data: &[u8]) -> anyhow::Result<Vec<String>> {
    let file = FileOptions::uncached().load(pdf_data)?;
    let resolver = file.resolver();
    let mut strings = Vec::new();

    for page in file.pages() {
        let page = page?;
        let cache = build_font_cache(&resolver, &page)?;

        let contents = if let Some(contents) = page.contents.as_ref() {
            contents
        } else {
            continue;
        };
        let mut current_font: Option<&MaybeRef<Font>> = None;
        for op in &contents.operations(&resolver)? {
            match op {
                Op::TextDraw { text } => {
                    debug!("Text {:?}", text);
                    if let Some(font) = current_font {
                        let text =
                            to_unicode_string(text, font).context("PdfString decode error")?;
                        if !text.is_empty() {
                            strings.push(text.into_owned());
                        }
                    }
                }
                Op::TextDrawAdjusted { array: _ } => {
                    debug!("TextDrawAdjusted: {:?}", op);
                }
                Op::TextFont { name, size: _ } => {
                    debug!("TextFont: {}", name);
                    current_font = cache.get_font(name.as_str());
                }
                Op::TextNewline => {
                    debug!("TextNewline: {:?}", op);
                }
                Op::GraphicsState { name: _ } => {
                    debug!("GraphicsState: {:?}", op);
                }
                _ => {
                    trace!("Other Operation: {:?}", op);
                }
            }
        }
    }
    Ok(strings)
}

fn to_unicode_string<'a>(
    pdf_string: &'a PdfString,
    font: &MaybeRef<Font>,
) -> anyhow::Result<Cow<'a, str>> {
    if let Some(encoding) = font.encoding() {
        match encoding.base {
            BaseEncoding::WinAnsiEncoding => {
                // WinAnsi -> Windows Code Page 1252
                let (text, encoding_used, had_errors) =
                    encoding_rs::WINDOWS_1252.decode(&pdf_string.data);
                ensure!(!had_errors, "decoding errors");
                ensure!(
                    encoding_rs::WINDOWS_1252 == encoding_used,
                    "used wrong encoding"
                );
                Ok(text)
            }
            _ => {
                bail!("encoding not supported: {:?}", encoding);
            }
        }
    } else {
        bail!("no encoding");
    }
}

fn build_font_cache(resolve: &impl Resolve, page: &Page) -> Result<FontCache, pdf::PdfError> {
    let mut cache = FontCache::new();
    let resources = page.resources.as_ref().unwrap();
    for (name, font) in &resources.fonts {
        cache.add_font(name.as_str().to_owned(), font.clone());
    }
    for gs in resources.graphics_states.values() {
        if let Some((font, _)) = gs.font {
            let font = resolve.get(font)?;
            cache.add_font(font.name.as_ref().unwrap().as_str().to_owned(), font.into());
        }
    }
    Ok(cache)
}
