mod pdf_extract;
mod receipt;

pub use crate::receipt::{parse_pdf, parse_pdf_from_data};
use anyhow::Context;
use shopkeeper_data::{Attachment, NewPurchase};

pub fn parse_pdf_and_add_attachment(
    data: Vec<u8>,
    attachment_name: String,
) -> anyhow::Result<NewPurchase> {
    let mut purchase = parse_pdf_from_data(&data).context("failed to parse PDF")?;
    let attachment = Attachment {
        content_type: "application/pdf".to_owned(),
        data,
        name: attachment_name,
    };
    purchase.attachments.push(attachment);
    Ok(purchase)
}
