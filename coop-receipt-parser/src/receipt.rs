use crate::pdf_extract;
use anyhow::{anyhow, bail, Context};
use chrono::{DateTime, Utc};
use chrono::{NaiveDateTime, TimeZone};
use chrono_tz::Europe::Zurich;
use log::debug;
use shopkeeper_data::{ItemData, Money, NewPurchase, PurchaseData};
use std::io::Read;

pub fn parse_pdf<R: Read>(source: &mut R) -> anyhow::Result<NewPurchase> {
    let mut data = Vec::new();
    source.read_to_end(&mut data)?;
    parse_pdf_from_data(&data)
}

pub fn parse_pdf_from_data(data: &[u8]) -> anyhow::Result<NewPurchase> {
    let lines = pdf_extract::extract_text(data)?;
    debug!("Parsing extracted text: {:?}", &lines);
    parse_text(&lines)
}

fn parse_text(lines: &[String]) -> anyhow::Result<NewPurchase> {
    let location = lines[0].clone();
    let purchase_time = parse_timestamp(lines)?;
    let data = PurchaseData {
        location,
        store: "Coop".to_owned(),
        purchase_time,
    };

    let mut start = 2;
    while lines[start] != "Artikel" && lines[start] != "Article" {
        start += 1;
        if start == lines.len() {
            bail!("Failed to find start in lines: {:?}", lines);
        }
    }
    expect_line(&lines[start + 1], "Menge", "Quantité")?;
    expect_line(&lines[start + 2], "Preis", "Prix")?;
    expect_line(&lines[start + 3], "Aktion", "Action")?;
    expect_line(&lines[start + 4], "Total", "Total")?;
    expect_line(&lines[start + 5], "Zusatz", "Infos")?;
    let end = lines[(start + 6)..]
        .iter()
        .position(|x| x == "Total CHF" || x == "Somme CHF" || x == "Total")
        .unwrap()
        + start
        + 6;

    let items = parse_items(&lines[(start + 6)..end])?;

    Ok(NewPurchase {
        data,
        items,
        attachments: Vec::new(),
    })
}

fn parse_timestamp(lines: &[String]) -> anyhow::Result<DateTime<Utc>> {
    let (first, second) = if lines[1].len() == 5 {
        (&lines[6], &lines[1])
    } else {
        (&lines[1], &lines[2])
    };
    let mut timestamp = format!("{} {}", first, second);
    timestamp.truncate(14);
    let naive_dt = NaiveDateTime::parse_from_str(&timestamp, "%d.%m.%y %H:%M")
        .with_context(|| format!("Failed to parse '{}' as timestamp", &timestamp))?;
    let tz_aware = Zurich
        .from_local_datetime(&naive_dt)
        .earliest()
        .expect("impossible datetime");
    Ok(tz_aware.with_timezone(&Utc))
}

fn parse_items(lines: &[String]) -> anyhow::Result<Vec<ItemData>> {
    debug!("Parse items: {:?}", lines);
    let mut indices: Vec<usize> = lines
        .iter()
        .enumerate()
        .filter_map(|(i, line)| if is_name(line) { Some(i) } else { None })
        .collect();
    indices.push(lines.len());
    indices
        .iter()
        .zip(indices.iter().skip(1))
        .filter_map(|(start, end)| parse_item(&lines[*start..*end]).transpose())
        .collect()
}

fn parse_item(lines: &[String]) -> anyhow::Result<Option<ItemData>> {
    debug!("Parse single item: {:?}", lines);
    // handle 'Seite X'
    if lines.len() < 2 || lines[0].starts_with("Seite") {
        return Ok(None);
    }
    let name = lines[0].clone();

    let price;
    let amount;
    let mut original_price = None;
    if lines.len() == 2 {
        // handle only total
        price = lines[1].parse()?;
        amount = 1.0;
    } else {
        amount = lines[1].parse()?;

        let mut rev = lines.iter().rev().fuse();
        let end = match (rev.next(), rev.next()) {
            (Some(x), Some(y)) if x == "A" && (y == "0" || y == "1") => lines.len() - 2,
            (Some(x), Some(y)) if x == "L" && y == "0" => lines.len() - 2,
            (Some(x), _) if x == "0" => lines.len() - 1,
            _ => lines.len(),
        };
        let number_count = lines[1..end]
            .iter()
            .take_while(|line| is_number(line))
            .count();
        if number_count == 3 {
            // normal item
            price = lines[3].parse()?;
        } else if number_count == 4 {
            // discounted item
            let original = lines[2].parse()?;
            price = lines[4].parse()?;
            let difference: Money = price - original;
            if difference != Money::ZERO {
                original_price = Some(original)
            }
        } else {
            bail!(
                "unexpected number_count = {}, lines = {:?}",
                number_count,
                lines
            );
        }
    }

    let item = ItemData {
        name,
        amount,
        price,
        original_price,
    };
    debug!("Item: {:#?}", &item);
    Ok(Some(item))
}

fn is_number(line: &str) -> bool {
    line.parse::<f32>().is_ok()
}

fn is_name(line: &str) -> bool {
    !is_number(line) && line.len() > 3
}

fn expect_line(line: &str, german: &str, french: &str) -> anyhow::Result<()> {
    if line == german || line == french {
        Ok(())
    } else {
        Err(anyhow!(
            "Expected '{}' or '{}' but found '{}'",
            german,
            french,
            line
        ))
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use chrono::DateTime;

    #[test]
    fn parse_text_basic() {
        let lines: Vec<String> = vec![
            "Zürich Triemli",
            "09.04.20 10:39",
            "09350 00358126 001",
            "0002580",
            "Artikel",
            "Menge",
            "Preis",
            "Aktion",
            "Total",
            "Zusatz",
            "Oecoplan Papiertasche D",
            "1",
            "0.30",
            "0.30",
            "#",
            "Coop Mayonnaise à la  française 265G",
            "1",
            "1.55",
            "1.55",
            "0",
            "Bio Bohnen 500g",
            "1",
            "6.95",
            "6.95",
            "0",
            "Total CHF",
            "14.75",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let data = parse_text(&lines).unwrap();
        assert_eq!(
            data,
            NewPurchase {
                data: PurchaseData {
                    location: "Zürich Triemli".to_owned(),
                    store: "Coop".to_owned(),
                    purchase_time: "2020-04-09T08:39:00Z".parse::<DateTime<Utc>>().unwrap()
                },
                items: vec![
                    ItemData {
                        name: "Oecoplan Papiertasche D".to_owned(),
                        amount: 1.0,
                        price: 0.3.into(),
                        original_price: None,
                    },
                    ItemData {
                        name: "Coop Mayonnaise à la  française 265G".to_owned(),
                        amount: 1.0,
                        price: 1.55.into(),
                        original_price: None,
                    },
                    ItemData {
                        name: "Bio Bohnen 500g".to_owned(),
                        amount: 1.0,
                        price: 6.95.into(),
                        original_price: None,
                    }
                ],
                attachments: Vec::new()
            }
        );
    }

    #[test]
    fn parse_text_discounted_items() {
        let lines: Vec<String> = vec![
            "Location",
            "08.01.21 16:12",
            "xxxxx",
            "Artikel",
            "Menge",
            "Preis",
            "Aktion",
            "Total",
            "Zusatz",
            "Pak-Choi St",
            "3",
            "2.85",
            "0.95",
            "2.85",
            "0",
            "Q&P IPS Vollmilch past. 1L",
            "2",
            "3.40",
            "1.70",
            "3.40",
            "0",
            "Coop Schweinsbrust gsn ca.750g",
            "0.812",
            "20.30",
            "15.20",
            "15.20",
            "L",
            "0",
            "Tempo Premium 4-lagig 24ROL",
            "2",
            "41.80",
            "20.90",
            "41.80",
            "A",
            "Naturabeef Siedfleisch durchz. ca. 700g",
            "0.751",
            "18.35",
            "13.15",
            "13.15",
            "A",
            "0",
            "Äpfel Gala I offen",
            "1.878",
            "6.20",
            "6.20",
            "6.20",
            "0",
            "Coop Speck geräuchert ca. 600g",
            "0.546",
            "11.15",
            "8.20",
            "L",
            "0",
            "Gratis Tempo",
            "-20.90",
            "Total CHF",
            "151.25",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let items = parse_text(&lines).unwrap().items;
        assert_eq!(
            items,
            vec![
                ItemData {
                    name: "Pak-Choi St".to_owned(),
                    amount: 3.0,
                    price: 2.85.into(),
                    original_price: None
                },
                ItemData {
                    name: "Q&P IPS Vollmilch past. 1L".to_owned(),
                    amount: 2.0,
                    price: 3.4.into(),
                    original_price: None
                },
                ItemData {
                    name: "Coop Schweinsbrust gsn ca.750g".to_owned(),
                    amount: 0.812,
                    price: 15.2.into(),
                    original_price: Some(20.3.into())
                },
                ItemData {
                    name: "Tempo Premium 4-lagig 24ROL".to_owned(),
                    amount: 2.0,
                    price: 41.8.into(),
                    original_price: None
                },
                ItemData {
                    name: "Naturabeef Siedfleisch durchz. ca. 700g".to_owned(),
                    amount: 0.751,
                    price: 13.15.into(),
                    original_price: Some(18.35.into())
                },
                ItemData {
                    name: "Äpfel Gala I offen".to_owned(),
                    amount: 1.878,
                    price: 6.2.into(),
                    original_price: None
                },
                ItemData {
                    name: "Coop Speck geräuchert ca. 600g".to_owned(),
                    amount: 0.546,
                    price: 8.2.into(),
                    original_price: None
                },
                ItemData {
                    name: "Gratis Tempo".to_owned(),
                    amount: 1.0,
                    price: (-20.9).into(),
                    original_price: None
                }
            ]
        );
    }

    #[test]
    fn parse_text_discounted_tomatoes() {
        let lines: Vec<String> = vec![
            "Location",
            "08.01.21 16:12",
            "xxxxx",
            "Artikel",
            "Menge",
            "Preis",
            "Aktion",
            "Total",
            "Zusatz",
            "Cherry Rispentomaten 500g",
            "1.0",
            "3.95",
            "2.35",
            "2.35",
            "0",
            "A",
            "Total",
            "CHF",
            "2.35",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let items = parse_text(&lines).unwrap().items;
        assert_eq!(
            items,
            vec![ItemData {
                name: "Cherry Rispentomaten 500g".to_owned(),
                amount: 1.0,
                price: 2.35.into(),
                original_price: Some(3.95.into())
            }]
        );
    }

    #[test]
    fn parse_text_discounted_toilet_paper() {
        let lines: Vec<String> = vec![
            "Location",
            "08.01.21 16:12",
            "xxxxx",
            "Artikel",
            "Menge",
            "Preis",
            "Aktion",
            "Total",
            "Zusatz",
            "Tempo 3-lagig weiss 32ROL",
            "1.0",
            "33.20",
            "19.90",
            "19.90",
            "1",
            "A",
            "Total",
            "CHF",
            "19.90",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let items = parse_text(&lines).unwrap().items;
        assert_eq!(
            items,
            vec![ItemData {
                name: "Tempo 3-lagig weiss 32ROL".to_owned(),
                amount: 1.0,
                price: 19.9.into(),
                original_price: Some(33.2.into())
            }]
        );
    }

    #[test]
    fn parse_text_french() {
        let lines: Vec<String> = vec![
            "Location",
            "08.01.21 16:12",
            "xxxxx",
            "Article",
            "Quantité",
            "Prix",
            "Action",
            "Total",
            "Infos",
            "Primagusto Fraises 450g",
            "1",
            "8.90",
            "8.90",
            "0",
            "Somme CHF",
            "8.90",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let data = parse_text(&lines).unwrap();
        assert_eq!(
            data,
            NewPurchase {
                data: PurchaseData {
                    location: "Location".to_owned(),
                    store: "Coop".to_owned(),
                    purchase_time: "2021-01-08T15:12:00Z".parse::<DateTime<Utc>>().unwrap()
                },
                items: vec![ItemData {
                    name: "Primagusto Fraises 450g".to_owned(),
                    amount: 1.0,
                    price: 8.9.into(),
                    original_price: None,
                }],
                attachments: Vec::new()
            }
        );
    }

    #[test]
    fn parse_timestamp_split() {
        let lines: Vec<String> = vec!["Zürich Triemli", "09.04.20", "10:39"]
            .into_iter()
            .map(|s| s.to_owned())
            .collect();
        let timestamp = parse_timestamp(&lines).unwrap();
        assert_eq!(
            timestamp,
            "2020-04-09T08:39:00Z".parse::<DateTime<Utc>>().unwrap()
        );
    }

    #[test]
    fn parse_timestamp_extra_data() {
        let lines: Vec<String> = vec!["Zürich Triemli", "09.04.20 10:39 0xxx", "00xxx"]
            .into_iter()
            .map(|s| s.to_owned())
            .collect();
        let timestamp = parse_timestamp(&lines).unwrap();
        assert_eq!(
            timestamp,
            "2020-04-09T08:39:00Z".parse::<DateTime<Utc>>().unwrap()
        );
    }

    #[test]
    fn parse_timestamp_ambiguous_datetime_chooses_earliest() {
        let lines: Vec<String> = vec!["Zürich Triemli", "25.10.20 02:37", "xxxx"]
            .into_iter()
            .map(|s| s.to_owned())
            .collect();
        let timestamp = parse_timestamp(&lines).unwrap();
        assert_eq!(
            timestamp,
            "2020-10-25T00:37:00Z".parse::<DateTime<Utc>>().unwrap()
        );
    }

    #[test]
    fn parse_timestamp_new_format() {
        let lines: Vec<String> = vec![
            "Zürich Triemli",
            "16:04",
            "08630",
            "00005090",
            "040",
            "0002736",
            "14.05.22",
            "Artikel",
        ]
        .into_iter()
        .map(|s| s.to_owned())
        .collect();
        let timestamp = parse_timestamp(&lines).unwrap();
        assert_eq!(
            timestamp,
            "2022-05-14T14:04:00Z".parse::<DateTime<Utc>>().unwrap()
        );
    }

    #[test]
    #[should_panic(expected = "impossible datetime")]
    fn parse_timestamp_impossible_datetime() {
        let lines: Vec<String> = vec!["Zürich Triemli", "29.03.20 02:37", "xxxx"]
            .into_iter()
            .map(|s| s.to_owned())
            .collect();
        let _result = parse_timestamp(&lines);
    }
}
