#![forbid(unsafe_code)]

mod receipt;

pub use crate::receipt::parse_csv;
