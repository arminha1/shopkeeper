use chrono::{NaiveDate, NaiveDateTime, NaiveTime, TimeZone, Utc};
use chrono_tz::Europe::Zurich;
use csv::ReaderBuilder;
use serde::{Deserialize, Deserializer};
use shopkeeper_data::{ItemData, Money, NewPurchase, PurchaseData};
use std::io::Read;

// ﻿Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz

#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Datum", deserialize_with = "deserialize_custom_date_format")]
    date: NaiveDate,
    #[serde(rename = "Zeit", deserialize_with = "deserialize_custom_time_format")]
    time: NaiveTime,
    #[serde(rename = "Filiale")]
    location: String,
    #[serde(rename = "Kassennummer")]
    cashier_number: u32,
    #[serde(rename = "Transaktionsnummer")]
    transaction_number: u32,
    #[serde(rename = "Artikel")]
    item_name: String,
    #[serde(rename = "Menge")]
    amount: f64,
    #[serde(rename = "Aktion")]
    sale: Money,
    #[serde(rename = "Umsatz")]
    price: Money,
}

fn deserialize_custom_date_format<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    NaiveDate::parse_from_str(&s, "%d.%m.%Y").map_err(serde::de::Error::custom)
}

fn deserialize_custom_time_format<'de, D>(deserializer: D) -> Result<NaiveTime, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    NaiveTime::parse_from_str(&s, "%H:%M:%S").map_err(serde::de::Error::custom)
}

pub fn parse_csv<R: Read>(source: R) -> anyhow::Result<Vec<NewPurchase>> {
    let mut rdr = ReaderBuilder::new().delimiter(b';').from_reader(source);
    let mut purchases = Vec::new();
    let mut current_records = Vec::new();

    for result in rdr.deserialize() {
        let record: Record = result?;
        if current_records
            .first()
            .map(|r| !same_purchase(r, &record))
            .unwrap_or(false)
        {
            purchases.push(create_purchase(&mut current_records));
        }
        current_records.push(record);
    }
    if !current_records.is_empty() {
        purchases.push(create_purchase(&mut current_records));
    }

    Ok(purchases)
}

fn same_purchase(r1: &Record, r2: &Record) -> bool {
    r1.cashier_number == r2.cashier_number
        && r1.transaction_number == r2.transaction_number
        && r1.date == r2.date
        && r1.time == r2.time
        && r1.location == r2.location
}

fn create_purchase(records: &mut Vec<Record>) -> NewPurchase {
    let data = create_purchase_data(&records[0]);
    let items = records.drain(..).map(create_item).collect();
    NewPurchase {
        data,
        items,
        attachments: Vec::new(),
    }
}

fn create_purchase_data(record: &Record) -> PurchaseData {
    let datetime = NaiveDateTime::new(record.date, record.time);
    let tz_aware = Zurich
        .from_local_datetime(&datetime)
        .earliest()
        .expect("impossible datetime");

    PurchaseData {
        store: "Migros".to_owned(),
        location: record.location.clone(),
        purchase_time: tz_aware.with_timezone(&Utc),
    }
}

fn create_item(record: Record) -> ItemData {
    let original_price = if record.sale == Money::ZERO {
        None
    } else {
        Some(record.price + record.sale.abs())
    };
    ItemData {
        name: record.item_name,
        amount: record.amount,
        price: record.price,
        original_price,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::DateTime;

    #[test]
    fn test_parse_csv_simple() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Zuckermais;0.58;0;3.85
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Crémant 55% 3x100g;1;0;6.15";
        let list = parse_csv(data.as_bytes()).unwrap();
        assert_eq!(list.len(), 1);
        assert_eq!(
            list[0],
            NewPurchase {
                data: PurchaseData {
                    store: "Migros".to_owned(),
                    location: "MM Affoltern a. A.".to_owned(),
                    purchase_time: "2020-10-09T07:49:22Z".parse::<DateTime<Utc>>().unwrap(),
                },
                items: vec![
                    ItemData {
                        name: "Zuckermais".to_owned(),
                        amount: 0.58,
                        price: 3.85.into(),
                        original_price: None,
                    },
                    ItemData {
                        name: "Crémant 55% 3x100g".to_owned(),
                        amount: 1.0,
                        price: 6.15.into(),
                        original_price: None,
                    }
                ],
                attachments: Vec::new(),
            }
        );
    }

    #[test]
    fn test_parse_csv_multiple_purchases() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Zuckermais;0.58;0;3.85
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Crémant 55% 3x100g;1;0;6.15
25.10.2020;11:15:36;MM Affoltern a. A.;435;65;Zuckermais;0.58;0;3.85";
        let list = parse_csv(data.as_bytes()).unwrap();
        println!("{:?}", list);
        assert_eq!(list.len(), 2);
        assert_eq!(list[0].items.len(), 2);
        assert_eq!(list[1].items.len(), 1);
    }

    #[test]
    fn test_parse_csv_sale() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Zuckermais;0.58;-0.15;3.85";
        let list = parse_csv(data.as_bytes()).unwrap();
        assert_eq!(list.len(), 1);
        assert_eq!(
            list[0].items,
            vec![ItemData {
                name: "Zuckermais".to_owned(),
                amount: 0.58,
                price: 3.85.into(),
                original_price: Some(4.00.into()),
            }]
        );
    }

    #[test]
    fn test_parse_csv_sale_positive_discount() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
09.10.2020;09:49:22;MM Affoltern a. A.;435;30;Zuckermais;0.58;0.15;3.85";
        let list = parse_csv(data.as_bytes()).unwrap();
        assert_eq!(list.len(), 1);
        assert_eq!(
            list[0].items,
            vec![ItemData {
                name: "Zuckermais".to_owned(),
                amount: 0.58,
                price: 3.85.into(),
                original_price: Some(4.00.into()),
            }]
        );
    }

    #[test]
    fn test_parse_csv_ambiguous_datetime_chooses_earliest() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
25.10.2020;02:15:20;MM Ambiguous;435;30;Zuckermais;1;0;2";
        let list = parse_csv(data.as_bytes()).unwrap();
        assert_eq!(list.len(), 1);
        assert_eq!(
            list[0].data.purchase_time,
            "2020-10-25T00:15:20Z".parse::<DateTime<Utc>>().unwrap()
        );
    }

    #[test]
    #[should_panic(expected = "impossible datetime")]
    fn test_parse_csv_impossible_datetime() {
        let data = "﻿\
Datum;Zeit;Filiale;Kassennummer;Transaktionsnummer;Artikel;Menge;Aktion;Umsatz
29.03.2020;02:10:15;MM Panics;435;30;Zuckermais;1;0;2";
        let _result = parse_csv(data.as_bytes());
    }
}
